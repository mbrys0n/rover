import re

import click

from models.grid import Grid
from models.position import Position, Orientation
from models.rover import Rover, RoverCommand


def parse_rover_input(rover_input: str):
    pattern = r"\(([-+]?\d+), ([-+]?\d+), ([NESW])\) ([LFR]+)"
    matches = re.match(pattern, rover_input)
    if not matches:
        raise ValueError(f"invalid rover input: {input}")
    x, y, orientation, commands = matches.groups()
    command_value_mapping = {command.value: command for command in RoverCommand}
    orientation_mapping = {
        orientation.value: orientation for orientation in Orientation
    }
    commands = [command_value_mapping.get(char, None) for char in commands]
    orientation = orientation_mapping.get(orientation, None)
    return Position(x=x, y=y), orientation, commands


def run_grid(grid_size, rover_input):
    grid_size_x, grid_size_y = map(int, grid_size.split(" "))
    rovers = []
    rover_commands = []
    for r_inp in rover_input:
        position, orientation, commands = parse_rover_input(r_inp)
        rover = Rover(position, orientation)
        rovers.append(rover)
        rover_commands.append(commands)

    grid = Grid(grid_size_x, grid_size_y, rovers)
    for i, commands in enumerate(rover_commands):
        for command in commands:
            grid.process_rover_command(i, command)
    print(grid)
    return grid


@click.group()
def cli(): ...


@cli.command()
@click.option(
    "--grid-size",
    prompt="Size of Grid (comma separated)",
    help="The size of the grid x,y",
)
@click.option(
    "--rover-input",
    prompt="Enter rover initial coords",
    help="L for Left, F for forward, R for rotate right",
    multiple=True,
)
def inline(grid_size, rover_input):
    run_grid(grid_size, rover_input)


@cli.command()
@click.option(
    "--path",
    prompt="Enter file path of file containing grid and rover config",
    help="use relative path",
)
def file(path):
    with open(path, "r") as f:
        content = f.readlines()
        run_grid(content[0], content[1:])


if __name__ == "__main__":
    cli()
