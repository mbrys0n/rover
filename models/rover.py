from enum import Enum
from typing import List

from models.position import Orientation, PositionDelta, Position


class RoverCommand(Enum):
    FORWARD = "F"
    ROTATE_LEFT = "L"
    ROTATE_RIGHT = "R"

    def __str__(self):
        return str(self.value)


class RoverStatus(Enum):
    OPERATIONAL = "OPERATIONAL"
    LOST = "LOST"

    def __str__(self):
        return str(self.value)


class Rover:
    position: Position
    orientation: Orientation
    status: RoverStatus = RoverStatus.OPERATIONAL
    historic_commands: List[RoverCommand] = []

    def __init__(self, initial_position: Position, initial_orientation: Orientation):
        self.position = initial_position
        self.orientation = initial_orientation

    def move_forward(self) -> Position:
        orientation_delta = {
            Orientation.NORTH: PositionDelta(x=0, y=1),
            Orientation.EAST: PositionDelta(x=1, y=0),
            Orientation.SOUTH: PositionDelta(x=0, y=-1),
            Orientation.WEST: PositionDelta(x=-1, y=0),
        }
        if position_delta := orientation_delta.get(self.orientation, None):
            self.position += position_delta
            return self.position
        else:
            raise NotImplementedError(f"Unsupported orientation: {self.orientation}")

    def rotate_left(self) -> Orientation:
        self.orientation -= 1
        return self.orientation

    def rotate_right(self) -> Orientation:
        self.orientation += 1
        return self.orientation

    def process_command(self, command: RoverCommand) -> None:
        command_mapping = {
            RoverCommand.ROTATE_LEFT: self.rotate_left,
            RoverCommand.ROTATE_RIGHT: self.rotate_right,
            RoverCommand.FORWARD: self.move_forward,
        }
        if execute_command := command_mapping.get(command, None):
            self.historic_commands.append(command)
            execute_command()
        else:
            raise NotImplementedError(f"Unknown command: {command}")

    def is_lost(self) -> bool:
        return self.status == RoverStatus.LOST

    def __str__(self):
        return f'({self.position.x},{self.position.y},{self.orientation}) {RoverStatus.LOST if self.is_lost() else ""}'
