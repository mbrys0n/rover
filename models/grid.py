from typing import List

from models.rover import RoverStatus, RoverCommand, Rover


class Grid:
    size_x: int
    size_y: int
    rovers: List[Rover]

    def __init__(self, size_x: int, size_y: int, rovers: List[Rover]):
        self.size_x = size_x
        self.size_y = size_y
        self.rovers = rovers

    def in_bounds(self, item) -> bool:
        return (0 <= item.position.x <= self.size_x) and (
            0 <= item.position.y <= self.size_y
        )

    def process_rover_command(self, rover_index: int, command: RoverCommand) -> None:
        rover = self.rovers[rover_index]
        rover.process_command(command)
        if not self.in_bounds(rover):
            rover.status = RoverStatus.LOST

    def __str__(self):
        return "\n".join(map(str, self.rovers))
