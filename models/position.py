from enum import Enum
from functools import singledispatchmethod

from pydantic import BaseModel


class PositionDelta(BaseModel):
    x: int
    y: int


class Position(BaseModel):
    x: int
    y: int

    @singledispatchmethod
    def __add__(self, obj):
        raise TypeError("Unsupported operand type(s) for +")

    @__add__.register(PositionDelta)
    def _(self, obj):
        return Position(x=self.x + obj.x, y=self.y + obj.y)

    @__add__.register(int)
    def _(self, obj):
        return Position(x=self.x + obj, y=self.y + obj)

    def __str__(self):
        return f"({self.x}, {self.y})"


class Orientation(Enum):
    NORTH = "N"
    EAST = "E"
    SOUTH = "S"
    WEST = "W"

    def __str__(self):
        return str(self.value)

    def __add__(self, other):
        """Overload the addition operator '+'"""
        orientations = list(Orientation)
        new_index = orientations.index(self) + 1
        return orientations[0 if new_index > len(orientations) - 1 else new_index]

    def __sub__(self, other):
        """Overload the subtraction operator '-'"""
        orientations = list(Orientation)
        new_index = orientations.index(self) - 1
        return orientations[len(orientations) - 1 if new_index < 0 else new_index]
