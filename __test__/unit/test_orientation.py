import pytest

from models.position import Orientation


class TestOrientation:
    @pytest.mark.parametrize(
        ("initial_orientation", "expected_orientation"),
        [
            (Orientation.NORTH, Orientation.EAST),
            (Orientation.EAST, Orientation.SOUTH),
            (Orientation.SOUTH, Orientation.WEST),
            (Orientation.WEST, Orientation.NORTH),
        ],
    )
    def test_clockwise_rotate_orientation(
        self, initial_orientation: Orientation, expected_orientation: Orientation
    ):
        orientation = initial_orientation + 1
        assert expected_orientation == orientation

    @pytest.mark.parametrize(
        ("initial_orientation", "expected_orientation"),
        [
            (Orientation.NORTH, Orientation.WEST),
            (Orientation.WEST, Orientation.SOUTH),
            (Orientation.SOUTH, Orientation.EAST),
            (Orientation.EAST, Orientation.NORTH),
        ],
    )
    def test_anticlockwise_rotate_orientation(
        self, initial_orientation: Orientation, expected_orientation: Orientation
    ):
        orientation = initial_orientation - 1
        assert expected_orientation == orientation
