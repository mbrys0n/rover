import pytest

from models.grid import Grid
from models.position import Position, Orientation
from models.rover import Rover, RoverCommand, RoverStatus


@pytest.fixture
def grid_with_rovers():
    position_1, position_2 = [Position(x=0, y=0), Position(x=5, y=5)]
    orientation_1, orientation_2 = [Orientation.NORTH, Orientation.EAST]
    rover_1 = Rover(initial_position=position_1, initial_orientation=orientation_1)
    rover_2 = Rover(initial_position=position_2, initial_orientation=orientation_2)
    grid = Grid(10, 10, rovers=[rover_1, rover_2])
    return grid


class TestGrid:
    def test_grid_out_of_bounds_x(self, grid_with_rovers):
        out_of_bound_rover_commands = [
            RoverCommand.ROTATE_LEFT,
            RoverCommand.FORWARD,
        ]
        for command in out_of_bound_rover_commands:
            grid_with_rovers.process_rover_command(0, command)

        rover = grid_with_rovers.rovers[0]
        assert rover.status == RoverStatus.LOST

    def test_grid_out_of_bounds_y(self, grid_with_rovers):
        out_of_bound_rover_commands = [
            RoverCommand.ROTATE_LEFT,
            RoverCommand.ROTATE_LEFT,
            RoverCommand.FORWARD,
        ]
        for command in out_of_bound_rover_commands:
            grid_with_rovers.process_rover_command(0, command)

        rover = grid_with_rovers.rovers[0]
        assert rover.status == RoverStatus.LOST

    def test_move_rover_in_bounds(self, grid_with_rovers):
        rover_commands = [
            RoverCommand.ROTATE_RIGHT,
            RoverCommand.FORWARD,
            RoverCommand.FORWARD,
            RoverCommand.FORWARD,
        ]
        for command in rover_commands:
            grid_with_rovers.process_rover_command(1, command)

        rover = grid_with_rovers.rovers[1]
        assert rover.status == RoverStatus.OPERATIONAL
