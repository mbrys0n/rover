from cli import parse_rover_input, run_grid
from models.position import Position, Orientation
from models.rover import RoverCommand

rover_input_1 = "(2, 3, E) LFRFF"


class TestCli:
    def test_parse_rover_input(self):
        position, orientation, commands = parse_rover_input(rover_input_1)
        assert position == Position(x=2, y=3)
        assert commands == [
            RoverCommand.ROTATE_LEFT,
            RoverCommand.FORWARD,
            RoverCommand.ROTATE_RIGHT,
            RoverCommand.FORWARD,
            RoverCommand.FORWARD,
        ]

    def test_cli_run_grid(self):
        grid = run_grid("5 5", [rover_input_1])
        assert grid.rovers[0].position == Position(x=4, y=4)
        assert grid.rovers[0].orientation == Orientation.EAST
