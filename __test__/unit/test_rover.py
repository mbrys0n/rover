import pytest

from models.position import Position, Orientation
from models.rover import Rover


class TestRover:
    @pytest.mark.parametrize(
        ("initial_orientation", "initial_position", "expected_position"),
        [
            (Orientation.NORTH, Position(x=5, y=5), Position(x=5, y=6)),
            (Orientation.EAST, Position(x=5, y=5), Position(x=6, y=5)),
            (Orientation.SOUTH, Position(x=5, y=5), Position(x=5, y=4)),
            (Orientation.WEST, Position(x=5, y=5), Position(x=4, y=5)),
        ],
        ids=["Move North", "Move East", "Move South", "Move West"],
    )
    def test_rover_move_forward(
        self,
        initial_orientation: Orientation,
        initial_position: Position,
        expected_position: Orientation,
    ):
        rover = Rover(
            initial_position=initial_position, initial_orientation=initial_orientation
        )
        rover.move_forward()
        assert expected_position == rover.position
