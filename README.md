# README

## Setup

`pip install -r requirements.txt`

## Usage

### Read from file

`python cli.py file --path rover_input.txt`

```
(4,4,E) 
(-1,6,N) LOST
```

### Inline

`python cli.py inline --grid-size "5 5" --rover-input "(2, 3, E) LFRFF" --rover-input "(4, 4, E) LRF"`

```
(4,4,E) 
(5,4,E)
```